/*
 * Copyright (c) 2012, salesforce.com, inc.
 * All rights reserved.
 * Redistribution and use of this software in source and binary forms, with or
 * without modification, are permitted provided that the following conditions
 * are met:
 * - Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * - Neither the name of salesforce.com, inc. nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission of salesforce.com, inc.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.rajesh.salesforce;

import java.io.UnsupportedEncodingException;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.rajesh.salesforce.adapters.ContactsListAdapter;
import com.rajesh.salesforce.callbacks.SaveCallback;
import com.rajesh.salesforce.networkmodel.SalesforceObject;
import com.rajesh.salesforce.utils.StringUtils;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.ui.sfnative.SalesforceActivity;

/**
 * Main activity
 */
public class MainActivity extends SalesforceActivity {
	private static final int ADD_CONTACT_DIALOG = 1000;

	ContactsListAdapter mAdapter;
	ListView mListView;

	/*
	 * (non-Javadoc)
	 * @see com.salesforce.androidsdk.ui.sfnative.SalesforceActivity#onCreate(android.os.Bundle)
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		mListView = (ListView)findViewById(R.id.contacts_list);
	}

	/*
	 * (non-Javadoc)
	 * @see com.salesforce.androidsdk.ui.sfnative.SalesforceActivity#onResume()
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	@Override 
	public void onResume() {
		findViewById(R.id.root).setVisibility(View.INVISIBLE);
		super.onResume();
	}		

	/*
	 * (non-Javadoc)
	 * @see com.salesforce.androidsdk.ui.sfnative.SalesforceActivity#onResume(com.salesforce.androidsdk.rest.RestClient)
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	@Override
	public void onResume(RestClient client) {
		if(client != null){
			SalesForceNucleus.chargeMe(client, getApplicationContext()); 
			findViewById(R.id.root).setVisibility(View.VISIBLE);
			if(mAdapter == null){
				mAdapter = new ContactsListAdapter(this, -1, null, "Contact");
				mListView.setAdapter(mAdapter);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreateDialog(int)
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		if(id == ADD_CONTACT_DIALOG){
			final Dialog dialog = new Dialog(this);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.create_dialog_view);
			dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
			dialog.findViewById(R.id.add_contact).setOnClickListener(new OnClickListener() {
				/*
				 * (non-Javadoc)
				 * @see android.view.View.OnClickListener#onClick(android.view.View)
				 * @since Jan 6, 2014
				 * @author rajeshcp
				 */
				@Override
				public void onClick(View view) {
					view.setEnabled(false);
					String firstName = ((EditText)dialog.findViewById(R.id.first_name)).getText().toString();
					String lastName  = ((EditText)dialog.findViewById(R.id.last_name)).getText().toString();
					String email     = ((EditText)dialog.findViewById(R.id.email)).getText().toString();
					if(StringUtils.isValidEmail(((EditText)dialog.findViewById(R.id.email)).getText())){
						if(firstName != null && firstName.length() > 0){
							if(lastName != null && lastName.length() > 0){
								SalesforceObject object = new SalesforceObject("Contact");
								object.put("FirstName", firstName);
								object.put("LastName", lastName);
								object.put("Email", email);
								object.save(new SaveCallback() {
									/*
									 * (non-Javadoc)
									 * @see com.rajesh.salesforce.callbacks.SaveCallback#finish(com.rajesh.salesforce.networkmodel.SalesforceObject, java.lang.Exception)
									 * @since Jan 7, 2014
									 * @author rajeshcp
									 */
									@Override
									public void finish(SalesforceObject object, Exception e) {
										dialog.findViewById(R.id.add_contact).setEnabled(true);
										if(object != null){
											mAdapter.add(object);
											mAdapter.notifyDataSetChanged();
											if(dialog.isShowing()){
												dialog.dismiss();
											}
										}else{
											Toast.makeText(dialog.getContext(), "Object not saved", Toast.LENGTH_SHORT).show();
										}
									}
								});
							}else{
								Toast.makeText(dialog.getContext(), "Last Name Cannot Be Blank", Toast.LENGTH_SHORT).show();
							}
						}else{
							Toast.makeText(dialog.getContext(), "First Name Cannot Be Blank", Toast.LENGTH_SHORT).show();
						}
					}else{
						Toast.makeText(dialog.getContext(), "Invalid Email", Toast.LENGTH_SHORT).show();
					}
				}
			});
			return dialog;
		}
		return super.onCreateDialog(id);
	}

	/**
	 * @param v of type 
	 * function which will initiate user logout 
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	public void onLogoutClick(View v) {
		SalesforceSDKManager.getInstance().logout(this);
	}


	/**
	 * @param v of type View
	 * function which will show the add contact dialog
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	public void onAddContactClick(View v) throws UnsupportedEncodingException {
		showDialog(ADD_CONTACT_DIALOG);
	}

}
