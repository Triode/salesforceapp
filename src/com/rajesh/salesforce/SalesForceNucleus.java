/**
 * 
 */
package com.rajesh.salesforce;

import android.content.Context;

import com.rajesh.salesforce.localcache.LocalCacheValenceShell;
import com.rajesh.salesforce.utils.HardwareUtils;
import com.salesforce.androidsdk.rest.RestClient;

/**
 * @author rajeshcp
 */
public class SalesForceNucleus {

	public static RestClient mRestClient;
	public static Context appContext;
	public static HardwareUtils mHardwareUtils;
	public static LocalCacheValenceShell mDBShell;

	/**  
	 * @return of type SalesForceNucleus
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SalesForceNucleus(final SingleTonEnforcer enforcer) {
	}

	/**
	 * @param mRestClient of type RestClient
	 * @return of type null 
	 * function which will create all the singleton instances for the app
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public static void chargeMe(final RestClient mRestClient, final Context appContext){
		SalesForceNucleus.mRestClient = mRestClient;
		SalesForceNucleus.appContext  = appContext;
		mHardwareUtils = HardwareUtils.getmInstance();
		mDBShell       = LocalCacheValenceShell.getmDBShell(appContext);
	}

	/**
	 * @return of type boolean 
	 * function which will bypass to the HardwarUtils to check the 
	 * network connetion
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public static boolean isNetworkAvailable(){
		return mHardwareUtils.isNetworkAvailable(appContext);
	}

	/**
	 * @param of type null
	 * @return of type null 
	 * function which will nullify all the instances associated
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public static void defuseMe(){
		SalesForceNucleus.mRestClient = null;
		appContext = null;
		mHardwareUtils = null;
		mDBShell.close();
		mDBShell = null;
	}

	/**
	 * @author rajeshcp
	 */
	class SingleTonEnforcer{

	}
}
