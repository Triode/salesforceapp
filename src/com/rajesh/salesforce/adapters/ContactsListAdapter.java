/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 6, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.adapters;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rajesh.salesforce.R;
import com.rajesh.salesforce.callbacks.DeleteCallback;
import com.rajesh.salesforce.networkmodel.SalesforceObject;

/**
 * @author rajeshcp
 */
public class ContactsListAdapter extends SalesforceAdapter implements OnClickListener{

	/**
	 * @author rajeshcp
	 */
	class ContactViewHolder{
		TextView mContactEmail;
		Button mDeleteButton;
	}

	/**
	 * @param context
	 * @param textViewResourceId
	 * @param objects
	 * @param objectName  
	 * @return of type ContactsListAdapter
	 * Constructor function
	 * @since Jan 6, 2014 
	 * @author rajeshcp
	 */
	public ContactsListAdapter(Context context, int textViewResourceId,
			List<SalesforceObject> objects, String objectName) {
		super(context, textViewResourceId, objects, objectName);
	}

	/* (non-Javadoc)
	 * @see com.rajesh.salesforce.adapters.SalesforceAdapter#doGetView(int, android.view.View, android.view.ViewGroup)
	 * @since Jan 6, 2014
	 * @author rajeshcp 
	 */
	@Override
	public View doGetView(int position, View converView, ViewGroup parent) {
		ContactViewHolder holder;
		if(converView == null){
			converView = View.inflate(getContext(), R.layout.contact_row, null);
			holder = new ContactViewHolder();
			holder.mContactEmail = (TextView)converView.findViewById(R.id.contact_email);
			holder.mDeleteButton = (Button)converView.findViewById(R.id.delete_contact);
			holder.mDeleteButton.setOnClickListener(this);
			converView.setTag(holder);
		}else{
			holder = (ContactViewHolder)converView.getTag();
		}
		final SalesforceObject contact = getItem(position);
		holder.mDeleteButton.setTag(position);
		if(contact.ismIsDeleting()){
			holder.mDeleteButton.setText(R.string.deleting_contact);
			holder.mDeleteButton.setEnabled(false);
		}else{
			holder.mDeleteButton.setText(R.string.delete_contact);
			holder.mDeleteButton.setEnabled(true);
		}
		String name = contact.getString("FirstName");
		holder.mContactEmail.setText((name == null) ? "Invalid Field" : name);
		return converView;
	}

	/* (non-Javadoc)
	 * @see com.rajesh.salesforce.adapters.SalesforceAdapter#provideFields()
	 * @since Jan 6, 2014
	 * @author rajeshcp 
	 */
	@Override
	public String[] provideFields() {
		return new String[]{"Email", "FirstName", "LastName"};
	}
	
	/*
	 * (non-Javadoc)
	 * @see android.widget.ArrayAdapter#add(java.lang.Object)
	 * @since Jan 7, 2014
	 * @author rajeshcp
	 */
	@Override
	public void add(SalesforceObject object) {
		mDataProvider.add(0, object);
	}
	
	

	/*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 * @since Jan 7, 2014
	 * @author rajeshcp
	 */
	@Override
	public void onClick(View view) {
		int position = (Integer)view.getTag();
		final SalesforceObject object = getItem(position);
		if(object != null){
			object.delete(new DeleteCallback() {
				/*
				 * (non-Javadoc)
				 * @see com.rajesh.salesforce.callbacks.DeleteCallback#onDelete(com.rajesh.salesforce.networkmodel.SalesforceObject, java.lang.Exception)
				 * @since Jan 7, 2014
				 * @author rajeshcp
				 */
				@Override
				public void onDelete(SalesforceObject object, Exception e) {
					if(object != null){
						mDataProvider.remove(object);
						Toast.makeText(getContext(), "Object Deleted", Toast.LENGTH_SHORT).show();
					}else{
						Toast.makeText(getContext(), "Object Not Deleted", Toast.LENGTH_SHORT).show();
					}
					notifyDataSetChanged();
				}
			});
			notifyDataSetChanged();
		}
	}

}
