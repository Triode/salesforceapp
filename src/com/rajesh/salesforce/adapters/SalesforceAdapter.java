/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 5, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.rajesh.salesforce.R;
import com.rajesh.salesforce.callbacks.QueryCallback;
import com.rajesh.salesforce.networkmodel.SalesforceObject;
import com.rajesh.salesforce.query.SalesforceQuery;

/**
 * @author rajeshcp
 */
public abstract class SalesforceAdapter extends ArrayAdapter<SalesforceObject> {

	/**
	 * @author rajeshcp
	 */
	private class ProgressViewHolder{
		ProgressBar mProgressBar;
		TextView mProgressText;
		Button mRetryButton;
		RelativeLayout mContainer;
	}

	/**
	 * @author rajeshcp
	 */
	private class LoadMoreViewHolder{
		TextView mProgressText;
	}

	protected boolean mIsLoading, mLastPageLoaded;


	static final int PROGRESS_VIEW_TYPE  = 0;
	static final int LOAD_MORE_VIEW_TYPE = 1;
	static final int LIST_ROW_VIEW_TYPE  = 2;

	List<SalesforceObject> mDataProvider;


	final int ITEMS_PER_PAGE = 15;

	String mClassName;

	/**
	 * @param context
	 * @param textViewResourceId
	 * @param objects
	 * @param objectName  
	 * @return of type SalesforceAdapter
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SalesforceAdapter(Context context, int textViewResourceId,
			List<SalesforceObject> objects, String objectName) {
		super(context, textViewResourceId, objects);
		this.mDataProvider = objects;
		mClassName = objectName;
		initLoad();
	}
	/**
	 * @param of type null 
	 * @return of type null
	 * function which will initiate the query fetch
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	private void initLoad(){
		mIsLoading = true;
		SalesforceQuery query = new SalesforceQuery(mClassName);
		query.limit(ITEMS_PER_PAGE);
		query.skip((mDataProvider == null) ? 0 : mDataProvider.size());
		query.select(provideFields());
		query.find(new QueryCallback() {
			/*
			 * (non-Javadoc)
			 * @see com.rajesh.salesforce.callbacks.QueryCallback#success(java.util.List, java.lang.Exception)
			 * @since Jan 6, 2014
			 * @author rajeshcp
			 */
			@Override
			public void success(List<SalesforceObject> list, Exception exception) {
				mIsLoading = false;
				if(list != null && list.size() > 0){
					mDataProvider = (mDataProvider == null) ? new ArrayList<SalesforceObject>() : mDataProvider;
					mDataProvider.addAll(list);
					mLastPageLoaded = (list.size() < ITEMS_PER_PAGE);
				}else{
					mLastPageLoaded = (mDataProvider != null && mDataProvider.size() > 0);
				}
				notifyDataSetChanged();
			}
		});
		notifyDataSetChanged();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 * @since Jan 5, 2014
	 * @author rajeshcp 
	 */
	@Override
	public int getCount() {
		return (mDataProvider == null) ? 1 : (!mLastPageLoaded) ? 
				(this.mDataProvider.size() + 1) : this.mDataProvider.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 * @since Jan 5, 2014
	 * @author rajeshcp 
	 */
	@Override
	public SalesforceObject getItem(int arg0) {
		return mDataProvider.get(arg0);
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 * @since Jan 5, 2014
	 * @author rajeshcp 
	 */
	@Override
	public long getItemId(int arg0) {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.BaseAdapter#getItemViewType(int)
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	@Override
	public int getItemViewType(int position) {
		return (mDataProvider == null) ? PROGRESS_VIEW_TYPE : 
			(position == mDataProvider.size()) ? LOAD_MORE_VIEW_TYPE : LIST_ROW_VIEW_TYPE;
	}

	/*
	 * (non-Javadoc)
	 * @see android.widget.BaseAdapter#getViewTypeCount()
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	@Override
	public int getViewTypeCount() {
		return 3;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 * @since Jan 5, 2014
	 * @author rajeshcp 
	 */
	@Override
	public View getView(int position, View converView, ViewGroup parent) {
		int itemViewType = getItemViewType(position);
		if(itemViewType == PROGRESS_VIEW_TYPE){
			ProgressViewHolder holder;
			if(converView == null){
				converView = View.inflate(getContext(), R.layout.expandable_progress_layout, null);
				holder = new ProgressViewHolder();
				holder.mProgressBar = (ProgressBar)converView.findViewById(R.id.progressBar);
				holder.mProgressText = (TextView)converView.findViewById(R.id.progress_text);
				holder.mRetryButton = (Button)converView.findViewById(R.id.retry_button);
				holder.mContainer = (RelativeLayout)converView.findViewById(R.id.progress_holder);
				converView.setTag(holder);
				holder.mRetryButton.setOnClickListener(new OnClickListener() {
					/*
					 * (non-Javadoc)
					 * @see android.view.View.OnClickListener#onClick(android.view.View)
					 * @since Jan 6, 2014
					 * @author rajeshcp
					 */
					@Override
					public void onClick(View arg0) {
						initLoad();
					}
				});
			}else{
				holder = (ProgressViewHolder)converView.getTag();
			}
			if(mIsLoading){
				holder.mRetryButton.setVisibility(View.GONE);
				holder.mProgressText.setText("Loading Records");
				holder.mProgressBar.setVisibility(View.VISIBLE);
			}else{
				holder.mRetryButton.setVisibility(View.VISIBLE);
				holder.mProgressText.setText("Failed To Load Resource From local or server Database");
				holder.mProgressBar.setVisibility(View.GONE);
			}
			LayoutParams params = (LayoutParams)holder.mContainer.getLayoutParams();
			params.height = parent.getMeasuredHeight();
			holder.mContainer.setLayoutParams(params);

		}else if(itemViewType == LOAD_MORE_VIEW_TYPE){
			LoadMoreViewHolder holder;
			if(converView == null){
				converView = View.inflate(getContext(), R.layout.loadmore_tab, null);
				holder = new LoadMoreViewHolder();
				holder.mProgressText = (TextView)converView.findViewById(R.id.progress_text);
				converView.setTag(holder);
				holder.mProgressText.setOnClickListener(new OnClickListener() {
					/*
					 * (non-Javadoc)
					 * @see android.view.View.OnClickListener#onClick(android.view.View)
					 * @since Jan 6, 2014
					 * @author rajeshcp
					 */
					@Override
					public void onClick(View v) {
						if(!mIsLoading){
							initLoad();
						}
					}
				});
			}else{
				holder = (LoadMoreViewHolder)converView.getTag();
			}
			holder.mProgressText.setText((mIsLoading) ? "loading..." : "Load More Records");
		}else{
			return doGetView(position, converView, parent);
		}
		return converView;
	}

	/**
	 * @param position
	 * @param arg1
	 * @param arg2
	 * @return of type 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public abstract View doGetView(int position, View converView, ViewGroup parent);

	/**
	 * @param mQuery of type SalesforceQuery
	 * function which will return the fields to be fetched from the server  
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	public abstract String[] provideFields();

}
