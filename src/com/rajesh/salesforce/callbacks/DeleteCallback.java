/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 7, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.callbacks;

import com.rajesh.salesforce.networkmodel.SalesforceObject;

/**
 * @author rajeshcp
 */
public abstract class DeleteCallback {

	/**
	 * @return of type DeleteCallback
	 * Constructor function
	 * @since Jan 7, 2014 
	 * @author rajeshcp
	 */
	public DeleteCallback() {
	}
	
	/**
	 * @param object of type SalesforceObject
	 * @param e of type Exception
	 * function which will called on success of delete request 
	 * @since Jan 7, 2014
	 * @author rajeshcp
	 */
	public abstract void onDelete(final SalesforceObject object, Exception e);
	

}
