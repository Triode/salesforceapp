package com.rajesh.salesforce.callbacks;

import java.util.List;

import com.rajesh.salesforce.networkmodel.SalesforceObject;

public abstract class QueryCallback {

	/**
	 * @return of type QueryCallback
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public QueryCallback() {
		
	}
	
	/**
	 * @param list of type List<SalesforceObject>
	 * function which will be called when the server respond 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public abstract void success(List<SalesforceObject> list, Exception exception);

}
