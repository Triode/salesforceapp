/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 5, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.callbacks;

import com.rajesh.salesforce.networkmodel.SalesforceObject;

/**
 * @author rajeshcp
 */
public abstract class SaveCallback {

	/** 
	 * @return of type SaveCallback
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SaveCallback() {
	}
	
	/**
	 * @param e of type Exception
	 * function which will be called after the finishing of save request 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public abstract void finish(final SalesforceObject object, final Exception e);

}
