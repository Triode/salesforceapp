/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 5, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.exceptions;
/**
 * @author rajeshcp
 */
public class SalesforceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *   
	 * @return of type SalesforceException
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SalesforceException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param detailMessage  
	 * @return of type SalesforceException
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SalesforceException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param throwable  
	 * @return of type SalesforceException
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SalesforceException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * @param detailMessage
	 * @param throwable  
	 * @return of type SalesforceException
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SalesforceException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

}
