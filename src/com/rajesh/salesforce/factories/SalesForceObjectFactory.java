/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 5, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.factories;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.database.Cursor;

import com.rajesh.salesforce.networkmodel.SalesforceObject;

/**
 * @author rajeshcp
 */
public class SalesForceObjectFactory {

	/**
	 * @return of type SalesForceObjectFactory
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SalesForceObjectFactory(final SingleTonEnforcer singleton) {
	}

	/**
	 * @author rajeshcp
	 */
	class SingleTonEnforcer{
	}

	/**
	 * @return of type List<SalesforceObject>
	 * function which will generate the object list from JSON
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public static List<SalesforceObject> getObjectListFromJSON(final JSONArray objectArray, String className, String[] fields) throws Exception{
		final List<SalesforceObject> result = new ArrayList<SalesforceObject>();
		for(int i = 0; i < objectArray.length(); i++){
			final SalesforceObject object = new SalesforceObject(className);
			final JSONObject jsonObject = objectArray.getJSONObject(i);
			for(int j = 0; j < fields.length; j++){
				object.setmObjectId(jsonObject.getString("Id"));
				if(jsonObject.has(fields[j])){
					object.put(fields[j], jsonObject.get(fields[j]));
				}
			}
			result.add(object);
		}
		return result;
	}
	/**
	 * @param cursor Cursor
	 * @return of type List<SalesforceObject>
	 * creates list of SalesforceObject from cursor
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	public static List<SalesforceObject> getObjectsFromCursor(final Cursor cursor, final String className){
		List<SalesforceObject> list = new ArrayList<SalesforceObject>();
		do{
			final SalesforceObject object = new SalesforceObject(className);
			for(int i = 0; i < cursor.getColumnCount(); i++){
				String name = cursor.getColumnName(i);
				if(name.equals("Id")){
					object.setmObjectId(cursor.getString(i));
				}else{
					object.put(name, cursor.getString(i));
				}
			}
			list.add(object);
			/*if(object.getmObjectId() != null){
				list.add(object);
			}*/
		}while(cursor.moveToNext());
		return list;
	}

}
