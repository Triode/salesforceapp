/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 6, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.localcache;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.rajesh.salesforce.localcache.orm.ObjectRelationalModel;

/**
 * @author rajeshcp
 */
public class LocalCacheValenceShell extends SQLiteOpenHelper {

	private static final String TAG = "LocalCacheValenceShell";

    private static final String DATABASE_NAME = "Salesfroce.db";
    private static final int DATABASE_VERSION = 1;
    
    private static LocalCacheValenceShell mDBShell;

    /**
	 * @param of type null
	 * @return mDBShell of type LocalCacheValenceShell
	 * getter function for mDBShell
	 * @since Jan 6, 2014 
	 * @author rajeshcp 
	 */
	public static LocalCacheValenceShell getmDBShell(final Context mContext) {
		mDBShell = (mDBShell == null) ? new LocalCacheValenceShell(mContext, new SingleTonEnforcer()) : mDBShell;
		return mDBShell;
	}

	/**
     * @param context  
     * @return of type LocalCacheValenceShell
     * Constructor function
     * @since Jan 6, 2014 
     * @author rajeshcp
     */
    public LocalCacheValenceShell(Context context, final SingleTonEnforcer enforcer) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /*
     * (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
     * @since Jan 6, 2014
     * @author rajeshcp
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.i(TAG, "Creating database [" + DATABASE_NAME + " v." + DATABASE_VERSION + "]...");
        sqLiteDatabase.execSQL(ObjectRelationalModel.CONTACT_CREATE);
    }

    /*
     * (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
     * @since Jan 6, 2014
     * @author rajeshcp
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.i(TAG, "Upgrading database ["+DATABASE_NAME+" v." + oldVersion+"] to ["+DATABASE_NAME+" v." + newVersion+"]...");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ObjectRelationalModel.CONTACT);
        onCreate(sqLiteDatabase);
    }
    /**
     * @author rajeshcp
     */
    private static class SingleTonEnforcer{
    	
    }

}
