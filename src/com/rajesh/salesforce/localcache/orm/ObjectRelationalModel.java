/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 6, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.localcache.orm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.rajesh.salesforce.SalesForceNucleus;
import com.rajesh.salesforce.factories.SalesForceObjectFactory;
import com.rajesh.salesforce.networkmodel.SalesforceObject;

/**
 * @author rajeshcp
 * class which will take care of all insert update get and delete db 
 * operations
 */
public class ObjectRelationalModel {

	public final static String TAG = "ObjectRelationalModel";
	public final static String CONTACT = "Contact";

	public final static String CONTACT_CREATE = "CREATE TABLE " + CONTACT + " (" +
			"Id TEXT,Email TEXT,FirstName TEXT, LastName TEXT)";

	/**  
	 * @return of type ContactORM
	 * Constructor function
	 * @since Jan 6, 2014 
	 * @author rajeshcp
	 */
	private ObjectRelationalModel() {
	}

	/**
	 * @param query of type String 
	 * @return of type List<SalesforceObject>
	 * function which will fetch all the records from db
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	public static List<SalesforceObject> findRecords(final String query, String className) throws Exception{
		SQLiteDatabase database = SalesForceNucleus.mDBShell.getReadableDatabase();
		Cursor cursor = database.rawQuery(query, null);
		cursor.moveToFirst();
		List<SalesforceObject> list = (cursor.getCount() > 0) ? SalesForceObjectFactory.getObjectsFromCursor(cursor, className) : new ArrayList<SalesforceObject>();
		cursor.close();
		return list;
	}

	/**
	 * @param object of type SalesforceObject
	 * function which will add the record to db if not exist 
	 * otherwise update it
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	public static void insertOrUpdate(final SalesforceObject object) throws Exception{
		SQLiteDatabase database = SalesForceNucleus.mDBShell.getWritableDatabase();
		ContentValues values = getContentValues(object);
		if(isRecordPresent(object)){
			Log.d(TAG, "Updating existing Record");
			database.update(object.getmClassName(), values, "Id=?", new String[] {object.getmObjectId()});
		}else{
			Log.d(TAG, "Inserting New Record");
			database.insert(object.getmClassName(), "null", values);
		}

	}

	/**
	 * @param objects of type List<SalesforceObject>
	 * function which will add the records to db if not exist
	 * otherwise update it
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	public static void insertOrUpdate(final List<SalesforceObject> objects) throws Exception{
		Iterator<SalesforceObject> iterator = objects.iterator();
		while(iterator.hasNext()){
			insertOrUpdate(iterator.next());
		}
	}

	/**
	 * @param object of type SalesforceObject
	 * function which will delete given record from db
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	public static void delete(final SalesforceObject object){
		SQLiteDatabase database = SalesForceNucleus.mDBShell.getWritableDatabase();
		if(database.delete(object.getmClassName(), "Id" + "='" + object.getmObjectId() + "'", null) > 0){
			Log.d(TAG, "Record Deleted");
		}else{
			Log.d(TAG, "Record Not Deleted");
		}
	}

	/**
	 * @param object of type SalesforceObject
	 * @return of type ContentValues
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	private static ContentValues getContentValues(final SalesforceObject object){
		final ContentValues values = new ContentValues();
		Iterator<String> iterator = object.getKeys().iterator();
		values.put("Id", object.getmObjectId());
		while (iterator.hasNext()) {
			final String key = iterator.next();
			if(object.getString(key) != null){
				values.put(key, object.getString(key));
			}
		}
		return values;
	}

	/**
	 * @param object of type object
	 * @return of type null
	 * function will check object exist or not 
	 * @since Jan 6, 2014
	 * @author rajeshcp
	 */
	private static boolean isRecordPresent(final SalesforceObject object) throws Exception{
		SQLiteDatabase database = SalesForceNucleus.mDBShell.getReadableDatabase();
		final String query = "SELECT COUNT(*) FROM " + object.getmClassName() +  " WHERE Id='" + object.getmObjectId() + "'";
		Cursor cursor = database.rawQuery(query, null);
		if(cursor != null){
			cursor.moveToFirst();
			int count= cursor.getInt(0);
			return (count > 0);
		}
		return false;
	}

}
