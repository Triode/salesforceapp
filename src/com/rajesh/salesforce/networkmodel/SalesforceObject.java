/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 5, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.networkmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.rajesh.salesforce.R;
import com.rajesh.salesforce.SalesForceNucleus;
import com.rajesh.salesforce.callbacks.DeleteCallback;
import com.rajesh.salesforce.callbacks.SaveCallback;
import com.rajesh.salesforce.localcache.orm.ObjectRelationalModel;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestClient.AsyncRequestCallback;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

/**
 * @author rajeshcp
 */
public class SalesforceObject implements Parcelable{

	private boolean mIsDeleting;
	/**
	 * @param of type null
	 * @return mIsDeleting of type boolean
	 * getter function for mIsDeleting
	 * @since Jan 7, 2014 
	 * @author rajeshcp 
	 */
	public boolean ismIsDeleting() {
		return mIsDeleting;
	}

	private ArrayList<String> mSupportedTypes = new ArrayList<String>(){
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		{
			add(String.class.getName());
			add(Integer.class.getName());
			add(Float.class.getName());
			add(Boolean.class.getName());
		}};

		private String mObjectId;

		/**
		 * @param of type null
		 * @return mObjectId of type String
		 * getter function for mObjectId
		 * @since Jan 6, 2014 
		 * @author rajeshcp 
		 */
		public String getmObjectId() {
			return mObjectId;
		}

		/**
		 * @param mObjectId of type String
		 * @return of type null
		 * setter function for mObjectId
		 * @since Jan 6, 2014
		 * @author rajeshcp 
		 */
		public void setmObjectId(String mObjectId) {
			this.mObjectId = mObjectId;
		}

		private String mClassName;

		/**
		 * @param of type null
		 * @return mClassName of type String
		 * getter function for mClassName
		 * @since Jan 6, 2014 
		 * @author rajeshcp 
		 */
		public String getmClassName() {
			return mClassName;
		}

		//Value determine the object is changed after the last fetch from server
		private boolean mDurty;
		/**
		 * @param of type null
		 * @return mDurty of type boolean
		 * getter function for mDurty
		 * @since Jan 6, 2014 
		 * @author rajeshcp 
		 */
		public boolean ismDurty() {
			return mDurty;
		}

		//To hold the fields
		HashMap<String, Object> mObjectMap;
		/** 
		 * @return of type SalesforceObject
		 * Constructor function
		 * @since Jan 5, 2014 
		 * @author rajeshcp
		 */
		public SalesforceObject(final String className) {
			this.mClassName = className;
			mObjectMap = new HashMap<String, Object>();
		}

		/**
		 * @params of type null 
		 * @return of type Set<String>
		 * getter function for keys
		 * @since Jan 6, 2014
		 * @author rajeshcp
		 */
		public Set<String> getKeys(){
			return (mObjectMap != null) ? mObjectMap.keySet() : null;
		}

		/**
		 * @param key of type String 
		 * @return of type String
		 * function which will get the string value for the key
		 * @since Jan 6, 2014
		 * @author rajeshcp
		 */
		public String getString(final String key){
			String result = null;
			if(mObjectMap != null){
				Object data = mObjectMap.get(key);
				result = (data != null && data instanceof String) ? (String)data : result;
			}
			return result;
		}

		/**
		 * @param key of type String 
		 * @param value of type 
		 * function which will put the values for the Object
		 * @since Jan 5, 2014
		 * @author rajeshcp
		 */
		public void put(final String key, final Object value){
			mObjectMap.put(key, value);
			mDurty = true;
		}

		/**
		 * @param callback of type DeleteCallback
		 * function which will initiate the delete API call 
		 * @since Jan 7, 2014
		 * @author rajeshcp
		 */
		public void delete(final DeleteCallback callback){
			mIsDeleting = true;
			final RestClient client = SalesForceNucleus.mRestClient;
			final String apiVersion = SalesForceNucleus.appContext.getString(R.string.api_version);
			RestRequest request = RestRequest.getRequestForDelete(apiVersion, mClassName,
					mObjectId);
			client.sendAsync(request, new AsyncRequestCallback() {
				/*
				 * (non-Javadoc)
				 * @see com.salesforce.androidsdk.rest.RestClient.AsyncRequestCallback#onSuccess(com.salesforce.androidsdk.rest.RestRequest, com.salesforce.androidsdk.rest.RestResponse)
				 * @since Jan 7, 2014
				 * @author rajeshcp
				 */
				@Override
				public void onSuccess(RestRequest request, RestResponse response) {
					mIsDeleting = false;
					try{
						Log.d("SalesForceObject", response.asString());
						setmObjectId(response.asJSONObject().getString("id"));
						callback.onDelete(SalesforceObject.this, null);
					}catch(Exception e){
						callback.onDelete(null, e);
					}
					ObjectRelationalModel.delete(SalesforceObject.this);
				}
				
				/*
				 * (non-Javadoc)
				 * @see com.salesforce.androidsdk.rest.RestClient.AsyncRequestCallback#onError(java.lang.Exception)
				 * @since Jan 7, 2014
				 * @author rajeshcp
				 */
				@Override
				public void onError(Exception exception) {
					mIsDeleting = false;
					callback.onDelete(null, exception);
				}
			});
		}

		/**
		 * @param of type null 
		 * @return of type null
		 * function which will delete the resource based on recursive strategy 
		 * @since Jan 7, 2014
		 * @author rajeshcp
		 */
		public void deleteEventually(){
			// TO DO
		}

		/**
		 * @param callback of type SaveCallback
		 * function which will initiate the asynchronous saveRequest
		 * @since Jan 5, 2014
		 * @author rajeshcp
		 */
		public void save(final SaveCallback callback){
			final RestClient clinet = SalesForceNucleus.mRestClient;
			final String apiVersion = SalesForceNucleus.appContext.getString(R.string.api_version);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> map = (HashMap<String, Object>)mObjectMap.clone();
			if(mObjectId != null){
			}
			try{
				RestRequest request = RestRequest.getRequestForCreate(apiVersion, mClassName,
						map);
				clinet.sendAsync(request, new AsyncRequestCallback() {
					/*
					 * (non-Javadoc)
					 * @see com.salesforce.androidsdk.rest.RestClient.AsyncRequestCallback#onSuccess(com.salesforce.androidsdk.rest.RestRequest, com.salesforce.androidsdk.rest.RestResponse)
					 * @since Jan 6, 2014
					 * @author rajeshcp
					 */
					@Override
					public void onSuccess(RestRequest request, RestResponse response) {
						try{
							Log.d("SalesForceObject", response.asString());
							setmObjectId(response.asJSONObject().getString("id"));
							callback.finish(SalesforceObject.this, null);
						}catch(Exception e){
							callback.finish(null, e);
						}
						try {
							ObjectRelationalModel.insertOrUpdate(SalesforceObject.this);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					/*
					 * (non-Javadoc)
					 * @see com.salesforce.androidsdk.rest.RestClient.AsyncRequestCallback#onError(java.lang.Exception)
					 * @since Jan 6, 2014
					 * @author rajeshcp
					 */
					@Override
					public void onError(Exception exception) {
						callback.finish(null, exception);
					}
				});
			}catch(Exception e){
				callback.finish(null, e);
			}
		}

		/**
		 * @param OF TYPE NULL 
		 * @return of type null
		 * function which will initiate the recursive save strategy  
		 * @since Jan 5, 2014
		 * @author rajeshcp
		 */
		public void saveEventually(){
		}

		/**
		 * @param callback of type SaveCallback
		 * function which will initiate the asynchronous saveRequest if the 
		 * object is modified
		 * @since Jan 5, 2014
		 * @author rajeshcp
		 */
		public void saveIfDurty(final SaveCallback callback){
			if(mDurty){
				save(callback);
			}else{
				callback.finish(this, null);
			}
		}

		/*
		 * (non-Javadoc)
		 * @see android.os.Parcelable#describeContents()
		 * @since Jan 5, 2014
		 * @author rajeshcp
		 */
		@Override
		public int describeContents() {
			return 0;
		}

		/*
		 * (non-Javadoc)
		 * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
		 * @since Jan 5, 2014
		 * @author rajeshcp
		 */
		@Override
		public void writeToParcel(Parcel dest, int flags) {

		}

}







