/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 5, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.rajesh.salesforce.R;
import com.rajesh.salesforce.SalesForceNucleus;
import com.rajesh.salesforce.callbacks.QueryCallback;
import com.rajesh.salesforce.factories.SalesForceObjectFactory;
import com.rajesh.salesforce.localcache.orm.ObjectRelationalModel;
import com.rajesh.salesforce.networkmodel.SalesforceObject;
import com.salesforce.androidsdk.rest.RestClient.AsyncRequestCallback;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;

/**
 * @author rajeshcp
 */
public class SalesforceQuery {

	ArrayList<String> mSupportedTypes = new ArrayList<String>(){/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
		add(String.class.getName());
		add(Integer.class.getName());
		add(Float.class.getName());
		add(Boolean.class.getName());
	}};
	
	//mSkip will skip number of records from the result 
	//mlimit will fetch only maximum mLimit number of records 
	int mSkip, mLimit;

	String mClassName;

	String[] mFields;

	//To hold the fields
	HashMap<String, Object> mFieldEqualToMap;


	/**
	 * @return of type SalesforceQuery
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public SalesforceQuery(final String className) {
		this.mClassName = className;
		mFieldEqualToMap = new HashMap<String, Object>();
	}

	/////////////////////////////////////////////
	///////////////Private Methods///////////////
	/////////////////////////////////////////////
	
	
	
	
	/**
	 * @param callback of type QueryCallback
	 * function which will fetch the data from the local database 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	private void fetchLocalData(final QueryCallback callback){
		try{
			final List<SalesforceObject> list = ObjectRelationalModel.findRecords(buildQuery(), mClassName);
			callback.success(list, null);
		}catch(Exception e){
			callback.success(null, e);
		}
		
	}
	
	/**
	 * @param mCallback of type QueryCallback
	 * function which will fetch the data from sales force server 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	private void fetchRemoteData(final QueryCallback mCallback){
		try{
			RestRequest restRequest = RestRequest.getRequestForQuery(SalesForceNucleus.appContext.getString(R.string.api_version), buildQuery());
			SalesForceNucleus.mRestClient.sendAsync(restRequest, new AsyncRequestCallback() {
				/*
				 * (non-Javadoc)
				 * @see com.salesforce.androidsdk.rest.RestClient.AsyncRequestCallback#onSuccess(com.salesforce.androidsdk.rest.RestRequest, com.salesforce.androidsdk.rest.RestResponse)
				 * @since Jan 5, 2014
				 * @author rajeshcp
				 */
				@Override
				public void onSuccess(RestRequest request, RestResponse response) {
					try{
						List<SalesforceObject> list = SalesForceObjectFactory.getObjectListFromJSON(response.asJSONObject().getJSONArray("records"), mClassName, mFields);
						mCallback.success(list, null);
						ObjectRelationalModel.insertOrUpdate(list);
					}catch(Exception e){
						mCallback.success(null, e);
					}
				}

				/*
				 * (non-Javadoc)
				 * @see com.salesforce.androidsdk.rest.RestClient.AsyncRequestCallback#onError(java.lang.Exception)
				 * @since Jan 5, 2014
				 * @author rajeshcp
				 */
				@Override
				public void onError(Exception exception) {
					mCallback.success(null, exception);
				}
			});
		}catch(Exception e){
			mCallback.success(null, e);
		}
	}
	
	
	/**
	 * @return of type String 
	 * function which will build the query string 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	private String buildQuery(){
		String query = "SELECT Id,";
		if(mFields != null && mFields.length > 0){
			for(int i = 0; i < mFields.length; i++){
				query += (i == 0) ? mFields[i] : ("," + mFields[i]);
			}
		}
		query +=  " FROM " + mClassName;
		if(mLimit > 0){
			query += " LIMIT " + String.valueOf(mLimit);
		}
		
		if(mSkip > 0){
			query += " OFFSET " + String.valueOf(mSkip);
		}
		return query;
	}

	/////////////////////////////////////////////
	///////////////Public Methods////////////////
	/////////////////////////////////////////////

	/**
	 * @param limit of type int 
	 * function which will set the limit for the records to be fetched 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public void limit(final int limit){
		mLimit = limit;
	}
	
	/**
	 * @param skip of type int 
	 * function which will set the number of records to be skipped 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public void skip(final int skip){
		mSkip = skip;
	}
	
	/**
	 * @param select of type String[]
	 * function which will set the columns to be fetched from server
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public void select(String[] columns){
		this.mFields = columns;
	}

	/**
	 * @param mCallback of type QueryCallback
	 * function which will initiate the fetch of records 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public void find(final QueryCallback mCallback){
		if(SalesForceNucleus.isNetworkAvailable()){
			fetchRemoteData(mCallback);
		}else{
			fetchLocalData(mCallback);
		}
	}

}
