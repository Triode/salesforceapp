/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 5, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * @author rajeshcp
 */
public class HardwareUtils {

	Context appContext;
	
	static HardwareUtils mInstance;
	/**
	 * @param of type null
	 * @return mInstance of type HardwareUtils
	 * getter function for mInstance
	 * @since Jan 5, 2014 
	 * @author rajeshcp 
	 */
	public static HardwareUtils getmInstance() {
		mInstance = (mInstance == null) ? new HardwareUtils(new SingletonEnforcer()) : mInstance;
		return mInstance;
	}

	/** 
	 * @return of type HardwareUtils
	 * Constructor function
	 * @since Jan 5, 2014 
	 * @author rajeshcp
	 */
	public HardwareUtils(final SingletonEnforcer enforcer) {
	}
	
	/**
	 * @param context of type Context
	 * @return of type boolean 
	 * function which will check the network is avialable or not 
	 * @since Jan 5, 2014
	 * @author rajeshcp
	 */
	public boolean isNetworkAvailable(Context context) {
	    ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

	    if (connectivity != null) {
	        NetworkInfo[] info = connectivity.getAllNetworkInfo();

	        if (info != null) {
	            for (int i = 0; i < info.length; i++) {
	                Log.i("HardwareUtils", info[i].getState().toString());
	                if (info[i].getState() == NetworkInfo.State.CONNECTED) {
	                    return true;
	                }
	            }
	        }
	    }
	    return false;
	}
	
	static class SingletonEnforcer{
		
	}

}
