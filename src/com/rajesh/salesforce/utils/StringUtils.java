/*
 * Copyright (c) 2014 Rajesh CP
 * All Rights Reserved.
 * @since Jan 7, 2014 
 * @author rajeshcp
 */
package com.rajesh.salesforce.utils;

import android.text.TextUtils;

/**
 * @author rajeshcp
 */
public class StringUtils {
	/**
	 *   
	 * @return of type StringUtils
	 * Constructor function
	 * @since Jan 7, 2014 
	 * @author rajeshcp
	 */
	public StringUtils() {
	}

	/**
	 * @param target of type CharSequence
	 * @return of type boolean 
	 * function which will validate the email 
	 * @since Jan 7, 2014
	 * @author rajeshcp
	 */
	public final static boolean isValidEmail(CharSequence target) {
		return ((!TextUtils.isEmpty(target)) && (android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()));
	}   


}
